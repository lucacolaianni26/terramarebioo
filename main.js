//NAVBAR
let navbar = document.querySelector('#navbar');
let links = document.querySelectorAll('.nav-link');
// LOGHI CATTURATI
let logoBlack = document.querySelector('#logoBlack');
let logoWhite = document.querySelector('#logoWhite');

//CATTURO I 3 SPAN CHE SI INCREMENTANO IN ORDINE
let firstNumber = document.querySelector('#firstNumber');
let secondNumber = document.querySelector('#secondNumber');
let thirdNumber = document.querySelector('#thirdNumber');


//Navbar che cambia colore grazie all'IF e si allarga
window.addEventListener('scroll' , ()=>{

    let scrolled = window.scrollY;

    if(scrolled > 0){
        navbar.style.backgroundColor = 'var(--ve)';
        navbar.style.minHeight = '80px';
        navbar.classList.add('navbar-dark');

        links.forEach( (link)=>{
            link.style.color = 'var(--w)'
        })

        logoBlack.classList.add('d-none');
        logoWhite.classList.remove('d-none');


    } else{
        navbar.style.backgroundColor = 'transparent';
        navbar.style.minHeight = '80px';
        navbar.classList.remove('navbar-dark');


        links.forEach( (link)=>{
            link.style.color = 'var(--b)'
        })

        logoBlack.classList.remove('d-none');
        logoWhite.classList.add('d-none');


    }
})

//INCREMENTO DEI NUMERI

function incrementNumber( finalNumber, element, timing ){

    let counter = 0;

    let interval = setInterval(()=>{

        if(counter < finalNumber){

            counter++;
            element.innerHTML = counter ;

        } else {
            clearInterval(interval)
        }

    }, timing);

}



//INTERSECTION OBSERVER (vede a che altezza del sito mi trovo)
//entries sono gli elementi da intercettare per far scattare la funzione e nel codice è rappresentato come un array


let confirm = true;

let observer = new IntersectionObserver( (entries)=>{
    entries.forEach((entry)=>{

    if(entry.isIntersecting && confirm){

        setTimeout(() => {

            incrementNumber(0, firstNumber, 1);
            incrementNumber(15, secondNumber, 200);
            incrementNumber(999, thirdNumber, 1);
   
        }, 1500);
    
        confirm = false;
        }

    })
})

observer.observe(firstNumber);

//ARRAY CON I PRODOTTI

let products = [
    {name: 'Spaghetti con cozze' , description: 'Gli spaghetti con le cozze sono un primo piatto semplice e gustosissimo, adatto a ogni stagione. In questa ricetta sono in bianco, si preparano con facilità. ' , image: './media/cozze.jpg' },
    {name: 'Pasta al pomodoro' , description: 'Gli spaghetti con le cozze sono un primo piatto semplice e gustosissimo, adatto a ogni stagione. In questa ricetta sono in bianco, si preparano con facilità.' , image: './media/pomodori.jpg' },
    {name: 'Cannoli con pollo' , description: 'Gli spaghetti con le cozze sono un primo piatto semplice e gustosissimo, adatto a ogni stagione. In questa ricetta sono in bianco, si preparano con facilità.' , image: './media/pollo.jpg' },

]

//CARDWRAPPER

let cardWrapper = document.querySelector('#cardWrapper');

products.forEach ((product)=>{

    let div = document.createElement('div');
    div.classList.add('col-12' , 'col-md-6', 'col-lg-4', 'mt-4')
    div.innerHTML = `

    <div class="card">
                <img src="${product.image}" class="card-img-top" alt="Immagine card">
                <div class="card-body">
                    <!-- qui ho messo la classe per sfumare + h3 con fw bold -->
                  <h5 class="card-title h3">${product.name}</h5>
                  <p class="card-text">${product.description}</p>
                </div>
              </div>
    
    `

    cardWrapper.appendChild(div);

})


var swiper = new Swiper(".mySwiper", {

    autoplay: {
        delay:2000,
        disableOnInteractio: false,
    },

    effect: "coverflow",
    grabCursor: true,
    centeredSlides: true,
    slidesPerView: "auto",
    coverflowEffect: {
      rotate: 50,
      stretch: 0,
      depth: 100,
      modifier: 1,
      slideShadows: true,
    },
    pagination: {
      el: ".swiper-pagination",
    },
  });